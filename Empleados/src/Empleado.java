
public class Empleado {	
	public static float calcularSalarioBruto(TipoEmpleado tipo, float ventasMes, float horasExtra) throws Exception {
		float salarioBruto = 0;
		
		if(tipo != null && ventasMes >= 0 && horasExtra >= 0) {
			if(tipo == TipoEmpleado.Vendedor) {
				salarioBruto = 1000;			
			}
			else {
				salarioBruto = 1500;
			}
			
			if(ventasMes >= 1500) {
				salarioBruto = salarioBruto + 200;
			}
			else {
				if(ventasMes >= 1000) {
					salarioBruto = salarioBruto + 100;
				}
			}			
			
			salarioBruto = salarioBruto + horasExtra * 20;
		}
		else {
			throw new Exception("Exception");
		}
		
		
		return salarioBruto;
	}
	
	public static float calcularSalarioNeto(float salarioBruto) throws Exception {
		float salarioNeto = 0;
		
		if(salarioBruto >= 0) {
			if(salarioBruto < 1000) {
				salarioNeto = salarioBruto;
			}
			else {
				if(salarioBruto >= 1000 && salarioBruto < 1500) {
					salarioNeto = salarioBruto - salarioBruto * 0.16f;
				}
				else {
					salarioNeto = salarioBruto - salarioBruto * 0.18f;
				}
			}
		}
		else {
			throw new Exception("Exception");
		}
		
		return salarioNeto;
	}
}
