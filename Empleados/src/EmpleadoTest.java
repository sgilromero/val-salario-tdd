import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EmpleadoTest {
	//Test Salario Bruto
	@Test
	public void testCalculaSalarioBruto1() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Vendedor, 2000.0f, 8.0f);
		float resultadoEsperado = 1360.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto2() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Vendedor, 1500.0f, 3.0f);
		float resultadoEsperado = 1260.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto3() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Vendedor, 1499.99f, 0.0f);
		float resultadoEsperado = 1100.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto4() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Encargado, 1250.0f, 8.0f);
		float resultadoEsperado = 1760.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto5() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Encargado, 1000.0f, 0.0f);
		float resultadoEsperado = 1600.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto6() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Encargado, 999.99f, 3.0f);
		float resultadoEsperado = 1560.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto7() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Encargado, 500.0f, 0.0f);
		float resultadoEsperado = 1500.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto8() throws Exception {
		float resultadoReal = Empleado.calcularSalarioBruto(TipoEmpleado.Encargado, 0.0f, 8.0f);
		float resultadoEsperado = 1660.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioBruto9() throws Exception {
		Assertions.assertThrows(Exception.class, () ->
		Empleado.calcularSalarioBruto(TipoEmpleado.Vendedor, -1.0f, 8.0f));
	}
	
	@Test
	public void testCalculaSalarioBruto10() throws Exception {
		Assertions.assertThrows(Exception.class, () ->
		Empleado.calcularSalarioBruto(TipoEmpleado.Vendedor, 1500.0f, -1.0f));
	}
	
	@Test
	public void testCalculaSalarioBruto11() throws Exception {
		Assertions.assertThrows(Exception.class, () ->
		Empleado.calcularSalarioBruto(null, 1500.0f, 8.0f));
	}

	//Test Salario Neto
	@Test
	public void testCalculaSalarioNeto1() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(2000.0f);
		float resultadoEsperado = 1640.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto2() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1500.0f);
		float resultadoEsperado = 1230.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto3() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1499.99f);
		float resultadoEsperado = 1259.9916f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto4() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1250.0f);
		float resultadoEsperado = 1050.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto5() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1000.0f);
		float resultadoEsperado = 840.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto6() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(999.99f);
		float resultadoEsperado = 999.99f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto7() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(500.0f);
		float resultadoEsperado = 500.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto8() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(0.0f);
		float resultadoEsperado = 0.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}
	
	@Test
	public void testCalculaSalarioNeto9() throws Exception {
		Assertions.assertThrows(Exception.class, () ->
		Empleado.calcularSalarioNeto(-1.0f));
	}
}
